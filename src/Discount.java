public interface Discount {
	
		public void checkDiscount(Cart cart);
		public double discount ();
}
